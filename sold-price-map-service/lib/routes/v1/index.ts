import express from "express";
import httpStatus from "http-status-codes";
import mapping from "./mapping/index";

const router = express.Router();

router.get("/v1/status", (_req, res) => res.sendStatus(httpStatus.OK));
router.get("/v1/mapping", (_req, res) => res.status(httpStatus.OK).json(mapping()));

export default router;
