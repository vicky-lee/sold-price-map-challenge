import ISoldPriceDataWithoutKeys from "./ISoldPriceDataWithoutKeys";
import ISoldPriceData from "./ISoldPriceData";

const addCoordinateKeys = (priceData: ISoldPriceDataWithoutKeys[]): ISoldPriceData => {
  const soldPriceMap: ISoldPriceData = {};

  priceData.forEach((priceData: ISoldPriceDataWithoutKeys) => {
    soldPriceMap[`${priceData.x}-${priceData.y}`] = { ...priceData };
  });

  return soldPriceMap;
}

export default addCoordinateKeys;