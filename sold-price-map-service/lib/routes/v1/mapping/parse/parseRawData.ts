import fs from "fs";
import ParsedData from "./ParsedData";

const parseRawData = (file: string): ParsedData => 
  fs.readFileSync(file, "utf8")
  .split("\n");

export default parseRawData;