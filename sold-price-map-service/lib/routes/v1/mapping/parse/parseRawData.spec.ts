jest.mock("fs");

import fs from "fs";
import parseRawData from "./parseRawData";
import { Chance } from "chance";

describe("parseRawData", () => {
  const fsMock = fs as jest.Mocked<typeof fs>;

  it("should call readFileSync and return expected data format", () => {
    const rawData = "60 23 1422640\n58 66 3653379\n61 62 5045331";
    const expectedParsedData = ["60 23 1422640", "58 66 3653379", "61 62 5045331"];

    fsMock.readFileSync.mockReturnValue(rawData);

    const file = Chance().string();
    const parsedData = parseRawData(file);

    expect(fsMock.readFileSync).toHaveBeenCalledWith(file, "utf8");
    expect(parsedData).toEqual(expectedParsedData);
  })
});