
import mockPercentileGroups from "../../../../test/mapping/percentileGroups/mockPercentileGroups.json";
jest.mock("./addPercentileIndexToPercentileGroups", () => () => mockPercentileGroups);

import unsortedParsedData from "../../../../test/mapping/percentileGroups/unsortedParsedData.json";
import priceDataWithPercentileGroups from "../../../../test/mapping/percentileGroups/priceDataWithPercentileGroups.json";
import assignPercentileGroupsToPriceData from "./assignPercentileGroupsToPriceData";

describe("assignPercentileGroupsToPriceData", () => {
  it("should sort, calculate and add the correct percentile group to the data", () => {
    const priceData = assignPercentileGroupsToPriceData(unsortedParsedData);
    expect(priceData).toEqual(priceDataWithPercentileGroups);
  })
});