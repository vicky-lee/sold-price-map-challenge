export default interface IPercentileGroup {
  id: number;
  percentileIndex? : number;
  maxPercentile: MAX_PERCENTILE
}

type MAX_PERCENTILE = 5 | 25 | 75 | 95 | 100;