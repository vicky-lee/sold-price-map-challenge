import addPercentileIndexToPercentileGroups from "./addPercentileIndexToPercentileGroups";
import expectedPercentileGroup from "../../../../test/mapping/percentileGroups/percentileGroupsGiven1000Total.json";

describe("addPercentileIndexToPercentileGroups", () => {
  it("should calculate and add the percentile index to the base percentile groups", () => {
    const percentileGroups = addPercentileIndexToPercentileGroups(1000);
    expect(percentileGroups).toEqual(expectedPercentileGroup);
  });
});