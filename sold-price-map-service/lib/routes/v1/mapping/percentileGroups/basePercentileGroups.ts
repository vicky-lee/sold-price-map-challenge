import IPercentileGroup from "./IPercentileGroup";

const basePercentileGroups: IPercentileGroup[] = [
  {
    id: 0,
    maxPercentile: 5
  },
  {
    id: 1,
    maxPercentile: 25
  },
  {
    id: 2,
    maxPercentile: 75
  },
  {
    id: 3,
    maxPercentile: 95
  },
  {
    id: 4,
    maxPercentile: 100
  }
]

export default basePercentileGroups;