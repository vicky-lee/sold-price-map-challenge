import basePercentileGroups from "./basePercentileGroups";
import IPercentileGroup from "./IPercentileGroup";

const calculatePercentileIndex = (percentile: number, total: number): number =>
  Math.ceil(percentile / 100 * total);

const addPercentileIndexToPercentileGroups = (total: number): IPercentileGroup[] => {
  basePercentileGroups.forEach((group: IPercentileGroup) => {
    group.percentileIndex = calculatePercentileIndex(group.maxPercentile, total)
  })

  return basePercentileGroups;
}

export default addPercentileIndexToPercentileGroups;