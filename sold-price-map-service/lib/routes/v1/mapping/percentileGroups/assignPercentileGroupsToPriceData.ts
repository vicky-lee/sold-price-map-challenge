import ISoldPriceDataWithoutKeys from "../ISoldPriceDataWithoutKeys";
import IPercentileGroup from "./IPercentileGroup";
import addPercentileIndexToPercentileGroups from "./addPercentileIndexToPercentileGroups";
import ParsedData from "../parse/ParsedData";

const sortByPriceData = (parsedData: ParsedData): ISoldPriceDataWithoutKeys[] => {
  return parsedData.map((price: string): ISoldPriceDataWithoutKeys => {
    let x, y, soldPrice;
    [x, y, soldPrice] = price.split(" ");
    return {
      x,
      y,
      price: soldPrice
    }
  })
  .sort((a: ISoldPriceDataWithoutKeys, b: ISoldPriceDataWithoutKeys) => +a.price - +b.price);
}

const assignPercentileGroupsToPriceData = (parsedData: ParsedData): ISoldPriceDataWithoutKeys[] => {
  const prices: ISoldPriceDataWithoutKeys[] = sortByPriceData(parsedData);
  const percentileGroups: IPercentileGroup[] = addPercentileIndexToPercentileGroups(prices.length);
  let prevIndex: number = 0;

  percentileGroups.forEach((group: IPercentileGroup) => {
    const pricesInThisGroup = prices.slice(prevIndex, group.percentileIndex);
    
    pricesInThisGroup.forEach((price: ISoldPriceDataWithoutKeys) => {
      price.percentileGroup = group.id as any
    })

    prevIndex = group.percentileIndex++;
  })

  return prices;
}

export default assignPercentileGroupsToPriceData;