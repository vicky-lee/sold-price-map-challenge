import ISoldPriceDataWithoutKeys from "./ISoldPriceDataWithoutKeys";

export default interface ISoldPriceData {
  [xy: string]: ISoldPriceDataWithoutKeys
}