import mapping from "./index";
jest.mock("fs");
import fs from "fs";

describe("mapping/index", () => {
  const fsMock = fs as jest.Mocked<typeof fs>;

  it("should return sold price data with x-y keys from parsed data", () => {
    const mockRawData = "6 80 8974276\n64 60 6738811\n57 0 7954704\n51 95 8535280\n50 97 9144966";

    const expectedSoldPriceData = {
      "50-97": {
        "x": "50",
        "y": "97",
        "price": "9144966",
        "percentileGroup": 3
      },
      "51-95": {
        "x": "51",
        "y": "95",
        "price": "8535280",
        "percentileGroup": 2
      },
      "57-0": {
        "x": "57",
        "y": "0",
        "price": "7954704",
        "percentileGroup": 1,
      },
      "6-80": {
        "x": "6",
        "y": "80",
        "price": "8974276",
        "percentileGroup": 2
      },
      "64-60": {
        "x": "64",
        "y": "60",
        "price": "6738811",
        "percentileGroup": 0
      }
    };

    fsMock.readFileSync.mockReturnValue(mockRawData);

    expect(mapping()).toEqual(expectedSoldPriceData);
  })
})
