import priceDataWithoutCoordinateKeys from "../../../test/mapping/addCoordinateKeys/priceDataWithoutCoordinateKeys.json";
import priceDataWithCoordinateKeys from "../../../test/mapping/addCoordinateKeys/priceDataWithCoordinateKeys.json";
import addCoordinateKeys from "./addCoordinateKeys";

describe("addCoordinateKeys", () => {
  it("should add an x-y index key to each datapoint", () => {
    const mappedData = addCoordinateKeys(priceDataWithoutCoordinateKeys);
    expect(mappedData).toEqual(priceDataWithCoordinateKeys);
  });
});