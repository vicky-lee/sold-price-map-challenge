import parseRawData from "./parse/parseRawData";
import assignPercentileGroupsToPriceData from "./percentileGroups/assignPercentileGroupsToPriceData";
import addCoordinateKeys from "./addCoordinateKeys";
import path from "path";
import ParsedData from "./parse/ParsedData";
import ISoldPriceDataWithoutKeys from "./ISoldPriceDataWithoutKeys";
import ISoldPriceData from "./ISoldPriceData";

// Hardcoding for now - assuming this was in production, we would either be grabbing this from:
// - The DB: in which the raw data would not be in this format in the first place
// - Some cloud storage such as S3: in which we would be using aws-sdk
const rawData = path.join(__dirname, "./sold-price-data.txt");

const mapping = () => {
  const parsedData: ParsedData = parseRawData(rawData);
  const priceDataWithPercentileGroups: ISoldPriceDataWithoutKeys[] = assignPercentileGroupsToPriceData(parsedData);
  const priceDataWithKeys: ISoldPriceData = addCoordinateKeys(priceDataWithPercentileGroups);

  return priceDataWithKeys;
};

export default mapping;