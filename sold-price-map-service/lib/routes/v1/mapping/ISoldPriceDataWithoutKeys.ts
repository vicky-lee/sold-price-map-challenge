export default interface ISoldPriceDataWithoutKeys {
  x: number | string;
  y: number | string;
  price: number | string;
  percentileGroup?: number
}