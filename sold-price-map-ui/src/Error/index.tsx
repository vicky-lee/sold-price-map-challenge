import React from "react";

export default () => (
  <div>There was an error getting your map, please try again later.</div>
)