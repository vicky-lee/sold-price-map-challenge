import React from "react";
import { get } from "lodash";
import IXY from "./IXY";
import IPriceData from "./IPriceData";

const GRID_DIM_INDEX: number = 99;

const percentile = (xy: string, prices: IPriceData) => {
  const price = get(prices, xy);

  return price ? `percentile-group-${price.percentileGroup}` : ""
}

const createGrid = (prices: IPriceData) => {
  let grid = []
  let xy: IXY;

  const updateXY = (x: number, y: number) => xy = {x,y};
  const xyId = () => `${xy.x}-${xy.y}`;

  for (let y = GRID_DIM_INDEX; y >= 0; y--) {
    let cell = []
    for (let x = 0; x <= GRID_DIM_INDEX; x++) {
      updateXY(x,y);
      cell.push(<div key={`cell-${xyId()}`} id={`$${xyId()}`} className={`cell ${percentile(xyId(), prices)}`}></div>)
    }
    grid.push(<div key={`row-${xyId()}`} className="row">{cell}</div>)
  }
  return grid
}

export default (prices: IPriceData) => <div id={"map-container"}>{createGrid(prices)}</div>