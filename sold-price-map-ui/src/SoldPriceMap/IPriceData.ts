import IXY from "./IXY";

interface IPartialPriceData {
  prices: number;
  percentileGroup: number;
}

export default interface IPriceData {
  xy: IXY & IPartialPriceData
}