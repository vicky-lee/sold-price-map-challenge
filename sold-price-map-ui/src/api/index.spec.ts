jest.mock("./client");
import { getPriceData } from "./index";
import * as api from "./client";
import { Chance } from "chance";

describe("api", () => {
  const soldPriceMock: any = {
    get: jest.fn()
  }
  const apiMock = api as jest.Mocked<typeof api>;
  apiMock.soldPrice.mockReturnValue(soldPriceMock);

  it("should call sold-price api and return the data", async () => {
    const data = Chance().string();
    soldPriceMock.get.mockReturnValue({
      data
    });

    const expectedResponse = await getPriceData();
    expect(soldPriceMock.get).toHaveBeenCalledWith("/v1/mapping");
    expect(expectedResponse).toEqual(data);
  });
});