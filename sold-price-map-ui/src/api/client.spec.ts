import { soldPrice } from "./client";

describe("client", () => {
  it("creates an axios instace for sold-price", () => {
    const api = soldPrice();
    expect(api.get).toBeInstanceOf(Function);
  })
});
