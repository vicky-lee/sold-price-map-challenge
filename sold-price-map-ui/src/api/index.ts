import { soldPrice } from "./client";

export const getPriceData = async () => {
  try {
    const prices = await soldPrice().get("/v1/mapping");
    return prices.data;
  }
  catch (e) {
    throw new Error(e);
  }
};
