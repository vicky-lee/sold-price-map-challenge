import React, { Component } from 'react';
import './App.css';
import './SoldPriceMap/soldPriceMap.css';
import SoldPriceMap from './SoldPriceMap';
import IPriceDatapoints from "./SoldPriceMap/IPriceData";
import Error from "./Error";

import * as api from "./api";
import Loading from "./Loading";

interface IState {
  ready: boolean;
  error: boolean;
  prices?: IPriceDatapoints;
}

class App extends Component {
  public state: IState;

  constructor(props: {}) {
    super(props);
    this.state = {
      ready: false,
      error: false
    };
  }

  private getPrices = async () => {
    try {
      const prices = await api.getPriceData();
      this.setState({ prices });
    }
    catch (e) {
      this.setState({ error: true })
    }
  }

  public componentDidMount = async () => {
    await this.getPrices();
    this.setState({ ready: true });
  }

  render() {
    return (
      <div className="container">
      <h1>Sold Price Map</h1>
      {this.state.error ? <Error /> : 
        this.state.ready ? <SoldPriceMap {...this.state.prices as IPriceDatapoints}/> : <Loading/>}
      </div>
    );
  }
}

export default App;
