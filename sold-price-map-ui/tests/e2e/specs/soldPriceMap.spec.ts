import soldPriceMap from "../pages/soldPriceMap";

fixture("Sold Price Map")
  .page(`${soldPriceMap.path}`)

test("Renders the map", async (t: TestController) => {
  await t.expect(soldPriceMap.map.exists).ok();
})
