import { Selector } from 'testcafe';

const path: string = `http://localhost:3008/`;
const map: Selector = Selector("#map-container");

export default {
  path, map
}